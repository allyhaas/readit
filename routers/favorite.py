from fastapi import APIRouter, Depends
from routers.schemas import FavoriteBase, FavoriteDisplay
from sqlalchemy.orm.session import Session
from db.database import get_db
from routers.schemas import UserAuth
from auth.oauth2 import get_current_user
from db import db_favorite
from auth import oauth2
from typing import List

router = APIRouter(prefix="/favorite", tags=["favorite"])


@router.post("", response_model=FavoriteDisplay)
def create_favorite(
    request: FavoriteBase,
    db: Session = Depends(get_db),
    current_user: UserAuth = Depends(get_current_user),
):
    return db_favorite.create(db, request)


@router.get("/all", response_model=List[FavoriteDisplay])
def posts(db: Session = Depends(get_db)):
    return db_favorite.get_all(db)


@router.get("/delete/{id}")
def delete(
    id: int,
    db: Session = Depends(get_db),
    current_user: UserAuth = Depends(get_current_user),
):
    return db_favorite.delete(db, id, current_user.id)
