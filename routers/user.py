from routers.schemas import UserBase, UserDisplay
from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm.session import Session
from db.database import get_db
from db import db_user
from typing import List


router = APIRouter(
    prefix='/user',
    tags=['user']
)

@router.post('', response_model=UserDisplay)
def create_user(request: UserBase, db: Session = Depends(get_db)):
    return db_user.create_user(db, request)

@router.get("/all", response_model=List[UserDisplay])
def get_all_users(db: Session = Depends(get_db)):
    users = db_user.get_all_users(db)
    return users

@router.get("/{username}", response_model=UserDisplay)
def get_user_by_username(username: str, db: Session = Depends(get_db)):
    user = db.query(db_user).filter(db_user.username == username).first()
    if not user:
        raise HTTPException(
            status_code=404,
            detail=f"User with username {username} not found",
        )
    return user
