import requests
from fastapi import APIRouter, Depends
from fastapi import Request
from queries.searches import (
    SearchIn,
    SearchQueries,
)

router = APIRouter(prefix="/search", tags=["search"])


@router.get("/api/books")
async def get_search(
    author: str | None = None,
    title: str | None = None,
    subject: str | None = None,
    repo: SearchQueries = Depends(),):
    search = SearchIn(
        author=author, title=title, subject=subject
    )
    return repo.search_books(search=search)
# def search_books(author: str, request: Request):
#     try:
#         # Set up the request URL
#         base_url = "https://www.googleapis.com/books/v1/volumes"
#         query_param = f"inauthor:{author}"
#         url = f"{base_url}?q={query_param}"
#         authHeader = "AIzaSyBlGvoZdSRapbc79nlrN34pdM-IkEF8ETM"  # Replace with your actual API key

#         # Set up the request headers
#         headers = {
#             "Authorization": authHeader,
#             "Content-Type": "application/json",
#         }

#         # Send the request to the Google Books API
#         response = requests.get(url, headers=headers)
#         response.raise_for_status()  # Raise an exception if the request was unsuccessful

#         # Extract the JSON data from the response
#         data = response.json()
#         title = data["items"][0]["volumeInfo"]["title"]
#         authors = data["items"][0]["volumeInfo"]["authors"]
#         img = data["items"][0]["volumeInfo"]["imageLinks"]["smallThumbnail"]

#         return title

#     except requests.exceptions.RequestException as e:
#         # Handle any request errors
#         return {"error": str(e)}
