from pydantic import BaseModel
from datetime import datetime
from typing import List


class UserBase(BaseModel):
    username: str
    email: str
    password: str


class UserDisplay(BaseModel):
    username: str
    email: str

    class Config:
        orm_mode = True


class Book(BaseModel):
    title: str
    author: str


class AuthorBase(BaseModel):
    name: str


class SearchResponse(BaseModel):
    total_items: int
    items: List[Book]


class FavoriteBase(BaseModel):
    author: str
    title: str
    image_url: str
    creator_id: str

# FOR FAVORITE DISPLAY
class User(BaseModel):
    username: str

    class Config:
        orm_mode = True

class FavoriteDisplay(BaseModel):
    id: int
    image_url: str
    timestamp: datetime
    user: User
    author: str
    title: str

    class Config:
        orm_mode = True


class UserAuth(BaseModel):
    id: int
    username: str
    email: str
