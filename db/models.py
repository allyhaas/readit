
from .database import Base
from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.orm import relationship
from sqlalchemy.sql.schema import ForeignKey



class DbUser(Base):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True, index=True)
    username = Column(String)
    email = Column(String)
    password = Column(String)
    favorites = relationship('DbFavorite', back_populates='user')



class DbFavorite(Base):
    __tablename__ = 'favorite'
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String)
    author = Column(String)
    image_url = Column(String)
    timestamp = Column(DateTime)
    user_id = Column(Integer, ForeignKey('user.id'))
    user = relationship('DbUser', back_populates='favorites')
