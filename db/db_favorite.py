from db.models import DbFavorite
from sqlalchemy.orm.session import Session
from routers.schemas import FavoriteBase
import datetime
from fastapi import HTTPException, status

def create(db: Session, request: FavoriteBase):
    new_favorite = DbFavorite(
        author=request.author,
        title=request.title,
        image_url=request.image_url,
        timestamp=datetime.datetime.now(),
        user_id=request.creator_id,
    )

    db.add(new_favorite)
    db.commit()
    db.refresh(new_favorite)
    return new_favorite


def get_all(db: Session):
    return db.query(DbFavorite).all()


def delete(db: Session, id: int, user_id: int):
    favorite = db.query(DbFavorite).filter(DbFavorite.id == id).first()
    if not favorite:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Favorite with an id {id} not found",
        )
    if favorite.user_id != user_id:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Only favorite creator can delete favorite",
        )

    db.delete(favorite)
    db.commit()
    return "ok"
