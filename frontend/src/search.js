import React, { useState } from "react";

function BookSearch() {
    const [results, setResults] = useState([]);
    const [searchCategory, setSearchCategory] = useState("");
    const [searchValue, setSearchValue] = useState("");

    const handleSubmit = (event) => {
        event.preventDefault();

        if (searchCategory && searchValue) {
            searchBooks();
        } else {
            console.log("Please select a search category and enter a value.");
        }
    };

    const searchBooks = () => {
        const queryParams = new URLSearchParams();
        queryParams.append(searchCategory, searchValue);

        const url = `http://localhost:8000/search/api/books?${queryParams}`;
        fetch(url)
            .then((response) => response.json())
            .then((data) => {
                setResults(data);
                console.log(data);
            })
            .catch((error) => {
                console.error(error);
            });
    };

    return (
        <>
            <form onSubmit={handleSubmit}>
                <div>
                    <label htmlFor="search-category">Search Category:</label>
                    <select
                        id="search-category"
                        value={searchCategory}
                        onChange={(e) => setSearchCategory(e.target.value)}
                    >
                        <option value="">Select Category</option>
                        <option value="author">Author</option>
                        <option value="title">Title</option>
                        <option value="subject">Subject</option>
                    </select>
                </div>
                <div>
                    <label htmlFor="search-value">Search Value:</label>
                    <input
                        id="search-value"
                        type="text"
                        value={searchValue}
                        onChange={(e) => setSearchValue(e.target.value)}
                    />
                </div>
                <button type="submit">Search</button>
            </form>

            <div id="results_table">
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Author</th>
                            <th>Image</th>
                        </tr>
                    </thead>
                    <tbody>
                        {results.map((book, index) => (
                            <tr key={index}>
                                <td>{book.title}</td>
                                <td>{book.authors.join(", ")}</td>
                                <td><img src={book.image} alt="" srcset="" /></td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </>
    );
}

export default BookSearch;
