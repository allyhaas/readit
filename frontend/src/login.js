import logo from "./logo.svg";
import "./App.css";
import React, { useState, useEffect } from "react";
import { Button, Modal, makeStyles, Input } from "@material-ui/core";
import { useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";

const BASE_URL = "http://localhost:8000/";

function getModalStyle() {
    const top = 50;
    const left = 50;

    return {
        top: `${top}%`,
        left: `${left}%`,
        transform: `translate(-${top}%, -${left}%)`,
    };
}

const useStyles = makeStyles((theme) => ({
    paper: {
        backgroundColor: theme.palette.background.paper,
        position: "absolute",
        width: 400,
        border: "2px solid #000",
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
}));

function LoginForm() {
    const classes = useStyles();
    const navigate = useNavigate();
    const [openSignIn, setOpenSignIn] = useState(false);
    const [openSignUp, setOpenSignUp] = useState(false);
    const [modalStyle, setModalStyle] = useState(getModalStyle);
    const [username, setUsername] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [authToken, setAuthToken] = useState("");
    const [authTokenType, setAuthTokenType] = useState("");
    const [userId, setUserId] = useState("");

    useEffect(() => {
        setAuthToken(window.localStorage.getItem("authToken"));
        setAuthTokenType(window.localStorage.getItem("authTokenType"));
        setUsername(window.localStorage.getItem("username"));
        setUserId(window.localStorage.getItem("userId"));
    }, []);

    useEffect(() => {
        authToken
            ? window.localStorage.setItem("authToken", authToken)
            : window.localStorage.removeItem("authToken");
        authTokenType
            ? window.localStorage.setItem("authTokenType", authTokenType)
            : window.localStorage.removeItem("authTokenType");
        username
            ? window.localStorage.setItem("username", username)
            : window.localStorage.removeItem("username");
        userId
            ? window.localStorage.setItem("userId", userId)
            : window.localStorage.removeItem("userId");
    }, [authToken, authTokenType, userId]);

    const signIn = (event) => {
        event?.preventDefault();

        let formData = new FormData();
        formData.append("username", username);
        formData.append("password", password);

        const requestOptions = {
            method: "POST",
            body: formData,
        };
        fetch(BASE_URL + "login", requestOptions)
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then((data) => {
                console.log(data);
                setAuthToken(data.access_token);
                setAuthTokenType(data.token_type);
                setUserId(data.user_id);
                setUsername(data.username);
           
            })
            .catch((error) => {
                console.log(error);
                alert("invalid username or password");
            });

        setOpenSignIn(false);
    };

    const signOut = (event) => {
        setAuthToken(null);
        setAuthTokenType(null);
        setUserId("");
        setUsername("");
    };

    const signUp = (event) => {
        event?.preventDefault();

        const json_string = JSON.stringify({
            username: username,
            email: email,
            password: password,
        });

        const requestOption = {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: json_string,
        };
        fetch(BASE_URL + "user", requestOption)
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                throw response;
            })
            .then((data) => {
                console.log(data);
                signIn();
            })
            .catch((error) => {
                console.log(error);
                alert(error);
            });

        setOpenSignUp(false);
    };

    return (
        <div className="app">
            <Modal open={openSignIn} onClose={() => setOpenSignIn(false)}>
                <div style={modalStyle} className={classes.paper}>
                    <form className="app_signin">
                        <center></center>
                        <Input
                            placeholder="username"
                            type="text"
                            value={username}
                            onChange={(e) => setUsername(e.target.value)}
                        />
                        <Input
                            placeholder="password"
                            type="password"
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                        />
                        <Button type="submit" onClick={signIn}>
                            Login
                        </Button>
                    </form>
                </div>
            </Modal>

            <Modal open={openSignUp} onClose={() => setOpenSignUp(false)}>
                <div style={modalStyle} className={classes.paper}>
                    <form className="app_signin">
                        <center></center>
                        <Input
                            placeholder="username"
                            type="text"
                            value={username}
                            onChange={(e) => setUsername(e.target.value)}
                        />
                        <Input
                            placeholder="email"
                            type="text"
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                        />
                        <Input
                            placeholder="password"
                            type="password"
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                        />
                        <Button type="submit" onClick={signUp}>
                            Sign up
                        </Button>
                    </form>
                </div>
            </Modal>
            <div className="app_header">
                {authToken ? (
                    <Button onClick={() => signOut()}>Logout</Button>
                ) : (
                    <div>
                        <Button onClick={() => setOpenSignIn(true)}>
                            Login
                        </Button>
                        <Button onClick={() => setOpenSignUp(true)}>
                            Signup
                        </Button>
                    </div>
                )}
            </div>
        </div>
    );
}

export default LoginForm;
