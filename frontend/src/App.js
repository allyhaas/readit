import logo from "./logo.svg";
import "./App.css";
import MainPage from "./MainPage";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Nav from "./Nav";
import BookSearch from "./search";
import LoginForm from "./login";

function App() {
    return (
        <BrowserRouter>
            <Nav />

            <Routes>
                <Route index element={<MainPage />} />
            </Routes>
            <Routes>
                <Route path="search" element={<BookSearch />} />
            </Routes>
            <Routes>
                <Route path="login" element={<LoginForm />} />
            </Routes>
        </BrowserRouter>
    );
}

export default App;
