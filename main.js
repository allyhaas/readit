var inputValue;

document.getElementById("myForm").addEventListener("submit", function (event) {
    event.preventDefault(); // Prevent form submission

    // Get the value from the input box
    inputValue = document.getElementById("authorLastName").value;

    // Use the value as needed
    console.log(inputValue);

    // Clear the input box after submitting
    document.getElementById("authorLastName").value = "";

    fetchData();
});

function fetchData() {
    const apiUrl = `https://www.googleapis.com/books/v1/volumes?q=inauthor:${inputValue}`;
    const authHeader = config.API_KEY;

    const requestOptions = {
        method: "GET",
        headers: {
            Authorization: authHeader,
            "Content-Type": "application/json",
        },
    };

    fetch(apiUrl, requestOptions)
        .then((response) => response.json())
        .then((data) => {
            // Call the displayResults function to show the data
            displayResults(data);
        })
        .catch((error) => {
            console.error("Error:", error);
        });
}

function displayResults(data) {
    // Get the container element to display the results
    var resultContainer = document.getElementById("resultContainer");

    // Clear the container before displaying new results
    resultContainer.innerHTML = "";

    // Iterate over the data and create HTML elements to display the information
    data.items.forEach((item) => {
        var bookTitle = item.volumeInfo.title;
        var thumbnailUrl = item.volumeInfo.imageLinks?.thumbnail; // Get the thumbnail URL (if available)

        // Create a container div for each book
        var bookElement = document.createElement("div");
        bookElement.classList.add("book");

        // Create an image element for the thumbnail
        var thumbnailElement = document.createElement("img");
        thumbnailElement.src = thumbnailUrl;
        thumbnailElement.alt = bookTitle;

        // Create a title element for the book
        var titleElement = document.createElement("h3");
        titleElement.textContent = bookTitle;

        // Append the thumbnail and title to the book container
        bookElement.appendChild(thumbnailElement);
        bookElement.appendChild(titleElement);

        // Append the book container to the result container
        resultContainer.appendChild(bookElement);
    });
}
