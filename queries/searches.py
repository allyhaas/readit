from pydantic import BaseModel
import requests
import os
from config import config


class SearchIn(BaseModel):
    author: str | None = None
    title: str | None = None
    subject: str | None = None


class SearchOut(SearchIn):
    id: str
    img: str


class SearchQueries:
    def search_books(self, search: SearchIn):
        url = "https://www.googleapis.com/books/v1/volumes"
        for key, value in search.dict().items():
            if value is not None:
                url += f"?q=in{key}:{value}"
        result = requests.get(url, headers={"API_KEY": config["API_KEY"]})
        print(url)
        data = result.json()
        search_results = []
        for item in data.get("items", []):
            volume_info = item.get("volumeInfo", {})
            title = volume_info.get("title")
            authors = volume_info.get("authors", [])
            subject = volume_info.get("subject")
            image_links = volume_info.get("imageLinks", {})
            image = image_links.get("thumbnail", "")

            search_results.append(
                {"title": title, "authors": authors, "subject": subject, "image": image}
            )

        return search_results
